import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import Header from './composant/Header';
import Bandeau from './composant/Bandeau';
import Main from './composant/Main';
import Footer from './composant/Footer';

ReactDOM.render(
  <React.StrictMode>
    <Header />
    <Bandeau />
    <Main />
    <Footer />
  </React.StrictMode>,
  document.getElementById('root')
);


