import logoFooter from '../assets/logo-footer.svg';
import './Footer.css';

let mainRed = "#ce0033";

const Stylefooter = {
  main: {
    width: '100%',
    height: '250px',
    background: mainRed,
    marginTop: '100px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  logo: {
    width: '250px'
  },
  list: {
    display: 'flex',
    gap: '5px',
    listStyle: 'none',
    color: 'white'
  }
}

function Footer() {

  return (
    <footer style={Stylefooter.main}>
      {logoFooter? <img src={logoFooter} alt="logo simplon" style={Stylefooter.logo}/>: <p>Erreur</p>}
      <ul style={Stylefooter.list}>
        <li>
          <a href="https://simplon.co/mentions-legales.html" target="_blank" rel="noreferrer ">
            Mentions légales
          </a>
        </li>
        <li>
          <a href="https://simplon.co/nos-actualites.html" target="_blank" rel="noreferrer">
            Nos actualités
          </a>
        </li>
        <li>
          <a href="https://simplon.co/nous-rejoindre.html" target="_blank" rel="noreferrer">
            Nous rejoindre
          </a>
        </li>
        <li>
          <a href="https://simplon.co/contact.html" target="_blank" rel="noreferrer">
            Contact
          </a>
        </li>
        <li>
          <a href="https://simplon.co/presse.html" target="_blank" rel="noreferrer">
            Presse
          </a>
        </li>
        <li>
          <a href="https://simplon.co/plan-du-site" target="_blank" rel="noreferrer">
            plan de site
          </a>
        </li>
        <li>
          <a href="https://www.simplonprod.co/" target="_blank" rel="noreferrer">
            Notre agence numérique
          </a>
        </li>
      </ul>
    </footer>
  );
}

export default Footer;
