import BandeauImage from "./../assets/BandeauImage.jpeg";
let mainRed = "#ce0033";

function Bandeau() {
  const StyleBandeau = {
    bandeauMain: {
      width: "100%",
      height: "400px",
      display: "flex",
      marginTop: "20px",
    },
    image: {
      width: "100%",
      height: "100%",
    },
    bandeau1: {
      width: "70%",
      height: "100%",
      display: "flex",
      backgroundColor: mainRed,
      justifyContent: "center",
      content: {
        width: "70%",
        display: "flex",
        flexDirection: "column",
        gap: "10px",
        marginTop: "50px",
        margin: "0px",
        padding: "0px",
        h1: {
          color: "white",
          fontSize: "40px",
        },
        p: {
          color: "white",
          fontSize: "18px",
        },
        a: {
            textDecoration: 'none',
            div: {
                padding: '10px',
                background: 'white',
                display: 'inline'
            }
        }
      },
    },
    bandeau2: {
      width: "30%",
      height: "100%",
    },
  };

  return (
    <div style={StyleBandeau.bandeauMain}>
      <div style={StyleBandeau.bandeau1}>
        <div style={StyleBandeau.bandeau1.content}>
          <h1 style={StyleBandeau.bandeau1.content.h1}>
            Simplon.co en Occitanie
          </h1>
          <p style={StyleBandeau.bandeau1.content.p}>
            Simplon.co est un réseau de Fabriques solidaires et inclusives qui
            proposent des formations gratuites aux métiers techniques du
            numérique en France et à l’étranger
          </p>
          <a href="https://occitanie.simplon.co/je-candidate" style={StyleBandeau.bandeau1.content.a}>
            <div style={StyleBandeau.bandeau1.content.a.div}>
                FORMATIONS OUVERTES
            </div>
          </a>
        </div>
      </div>
      <div style={StyleBandeau.bandeau2}>
        <img
          src={BandeauImage}
          alt="Simplon Bureau"
          style={StyleBandeau.image}
        />
      </div>
    </div>
  );
}

export default Bandeau;
