import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import "./map.css";
import Info from "./Info";
import Fabriques from "./fabriques.json";
import React, { useState } from "react";
import "leaflet/dist/leaflet.css";
import Position from "./Position.jsx";
import Icon from'./IconFix';

const StyleMain = {
  container: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  h2: {
    fontSize: "30px",
    marginTop: "30px",
  },
  wrapper: {
    width: "1000px",
    height: "500px",
    display: "flex",
    flexDirection: "row-reverse",
    background: "#f1f5f8",
  },
};


function Main() {

  const [i, setI] = useState(0);
  const [meteoInfos, setMeteo] = useState();
  function changerIndex(index) {
    setI(index);
  }
  function meteo(lat, long) {
      fetch('https://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + long + '&units=metric&lang=fr&appid=a69aeb54d577814c3b66d7bf64658904')
      .then(response => {
        let rep = response.json()
        console.log(rep)
        return rep
      })
      .then((obj) => {
        setMeteo(obj)
      })
      .catch(e => console.error(e))
  
  }

  return (
    <div style={StyleMain.container}>
      <h2 style={StyleMain.h2}>Simplon près de chez vous</h2>
      <div style={StyleMain.wrapper}>
        <MapContainer center={[44, 3]} zoom={7}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Position />
          {Fabriques.map((fab, index) => {
            return (
              <Marker
                position={[fab.latitude, fab.longitude]}
                key={index}
                icon={Icon}
                eventHandlers={{
                  click: () => {
                    changerIndex(index);
                    meteo(fab.latitude, fab.longitude);
                  },
                }}
              >
                <Popup>
                  <p>Température : {(meteoInfos)? meteoInfos.main.temp : ""}°C</p>
                  <p>Humidité : {(meteoInfos)? meteoInfos.main.humidity : ""}%</p>
                  <p>Vent : {(meteoInfos)? meteoInfos.wind.speed : ""}Km/h</p>
                  <p>{(meteoInfos)? meteoInfos.weather[0].description : ""}</p>
                  <img src={(meteoInfos)? `https://openweathermap.org/img/wn/${meteoInfos.weather[0].icon}@2x.png`:""} alt="icone météo" />
                </Popup>
              </Marker>
            );
          })}
        </MapContainer>
        <Info num={i} fabriques={Fabriques}/>
      </div>
    </div>
  );
}

export default Main;