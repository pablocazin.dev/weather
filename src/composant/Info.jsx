const StyleInfo = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '400px',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  wrapper: {
    width: '80%',
    borderTop: '4px solid #ce0033',
    background: 'white'
  },
  ville: {
    fontSize: '40px',
    textAlign: 'center'
  },
  text: {
    textAlign: 'center'
  }
}

function Info(props) {
  let fabriques = props.fabriques;
  let index = props.num;
  return (
    <div style={StyleInfo.container}>
      <div style={StyleInfo.wrapper}>
        <p style={StyleInfo.ville}>{fabriques[index].nom}</p>
        <p style={StyleInfo.text}>{fabriques[index].infos}</p>
        <p style={StyleInfo.text}>{fabriques[index].formation}</p>
      </div>
    </div>
  );
}
  
export default Info;

