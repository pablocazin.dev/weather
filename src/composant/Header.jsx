import logo from "./../assets/logo.png"
import facebooklogo from"./../assets/facebook.svg"
import linkedinlogo from"./../assets/linkedin.svg"
import twitterlogo from"./../assets/twitter.svg"

function Header() {
  const StyleHeader = {
    width: "100%",
    height: "150px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  };

  const StyleHeader1 = {
    width: "70%",
    height: "25%",
    borderBottom: "1px solid grey",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  };

  const StyleHeader2 = {
    width: "70%",
    height: "75%",
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "space-between"
  };

  const StyleHeaderLogo = {
    width: "200px",
  };

  const StyleHeader2Nav = {
    display: "flex",
    gap: "20px",
    listStyle: "none"
  }

  const SocialLinks = {
    facebook: "https://www.facebook.com/simplon.occitanie",
    linkedin: "https://www.linkedin.com/company/simplon-occitanie/",
    twitter: "https://twitter.com/Simplon_Occ"
  }

  return (
    <header style={StyleHeader}>
      <div style={StyleHeader1}>
        <p>Simplon.co</p>
        <ul style={StyleHeader2Nav}>
            <li>
              <a href={SocialLinks.facebook} target="_blank" rel="noreferrer">
                 <img src={facebooklogo} alt="Facebook"/>
              </a>
            </li>
            <li>
              <a href={SocialLinks.linkedin} target="_blank" rel="noreferrer">
                <img src={linkedinlogo} alt="Linkedin"/>
              </a>
            </li>
            <li>
              <a href={SocialLinks.twitter} target="_blank" rel="noreferrer">
                <img src={twitterlogo} alt="Twitter"/>
              </a>
            </li>
          </ul>
      </div>
      <div style={StyleHeader2}>
        <img
          style={StyleHeaderLogo}
          src={logo} 
          alt="logo"
        />
        <nav>
          <ul style={StyleHeader2Nav}>
            <li>
              vous voulez
            </li>
            <li>
              welcode
            </li>
            <li>
              nos actualités
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default Header;
